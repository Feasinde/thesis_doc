\chapter{Introduction}
\label{ch:introduction}

\section{Motivation} % (fold)
\label{sec:motivation}
	
Understanding the rationale behind a particular ruling made by a judge is a complex task that requires formal legal training in the relevant case law. Nevertheless, the rulings are still made using traditional methods of human discourse and reasoning, including default logic \parencite{walker2015representing}, deontic logic \parencite{dragoni2018nlprule}, and rhetoric \parencite{wald1995rhetoric}. In particular, knowing all the relevant facts surrounding a case is of the utmost importance to understand the outcome of a ruling, as they are necessary to arrive at the best-possible decision, since facts are what give way to what is usually called the \textit{Best Evidence} \parencite{stein2005foundations,nance1987best}.

Within a ruling, however, determining what constitutes a fact, as opposed to other types of content that motivate and illustrate a judge's decision, is also a matter that requires formal training. Indeed, a variety of linguistic factors, such as specialised terminology, textual structure, linguistic register, as well as domain knowledge, make the ruling stray from more general-domain texts. As such, fact extraction from legal texts is time consuming, expensive, and requires legal expertise. Additionally, as \textcite{westermann2019using} have shown, even amongst trained experts, inter-annotator agreement tends to be low. For example, in the task of labelling a corpus of rulings with a pre-established set of labels on the ruling's subject matter, \textcite{westermann2019using} reported low inter-annotator agreement and suggested that its cause might be the general vagueness and lack of explicit reasoning in the texts.

\subsection{What is a \textit{Fact}?} % (fold)
\label{sub:what_is_a_fact_}

There are several definitions of what constitutes a \textit{fact}, many of which are epistemological in nature. Here, we constrain the working definition to the realm of Law. According to the Merriam-Webster dictionary's legal definition \parencite{merriamWebsterFact}, a \textbf{fact} is:

\begin{quote}
	``Any of the circumstances of a case that exist or are alleged to exist in reality : a thing whose actual occurrence or existence is to be determined by the evidence presented at trial.''
\end{quote}

According to ALM's Legal Dictionary \parencite{almFact}, a fact is:

\begin{quote}
	``An actual thing or happening, which must be proved at trial by presentation of evidence and which is evaluated by the finder of fact (a jury in a jury trial, or by the judge if he/she sits without a jury).''
\end{quote}

Besides these general definitions, there are different kinds of facts, depending on the application and relevance of the legal context in which it plays a role (eg Merriam-Webster's Legal Dictionary \parencite{merriamWebsterFact} presents definitions for the terms \textit{Adjudicative Fact, Collateral Fact, Constitutional Fact}, etc). However, this thesis will deal with the general concept of a fact, as it stands in contrast with an \textit{analysis} and any other reason for acting that might guide the decision made by a judge.

Facts alone do not determine the outcome of a decision; they must be accompanied by an adequate \textit{classification}  because not all facts are relevant to the issue at hand \parencite[page 23]{hage2017introduction}. Nevertheless, facts are the basis for any kind of judicial application of the rules. Consider the example taken from \parencite{hage2017introduction}:

\begin{center}
\begin{tabular}{m{0.8\linewidth}}
\begin{example}
	\label{ex:pierre}
	Pierre, 18 years old, visits the house of his neighbors. He is distracted by the daughter of the house, does not look where he is walking, stumbles over the carpet, and falls against an antique Chinese vase, which breaks as a consequence. The value of the vase was €3,000. Must Pierre (or his insurance) pay his neighbor €3,000?
\end{example}
\end{tabular}
\end{center}


 To answer the question from Example~\ref{ex:pierre}, we can construe the facts as follows:

\begin{itemize}
	\item Pierre is 18 years old.
	\item He visited the neighbours' house.
	\item He became distracted because of the neighbours' daughter.
	\item He stumbled over the carpet and fell against an antique Chinese vase.
	\item The vase broke.
	\item The vase was valued at €3,000.
\end{itemize}

Using these facts, \textcite{hage2017introduction} determine that, if the rule in question is:

\begin{quote}
	``IF somebody acted wrongfully toward another person, and if they thereby caused damage to this other person, THEN they must compensate this damage.''
\end{quote}

then Pierre has ``acted wrongfully toward his neighbors and thereby he caused damage to his neighbors'', and therefore the logical conclusion is that ``Pierre must compensate the damage to his neighbors.''

Even using a relatively straightforward definition, however, determining whether a given statement, assuming complete truthfulness, actually contains a fact is not always simple, especially since, as with any specialised field, a technical document will follow a written convention that may not be as straightforward as the one shown in Example~\ref{ex:pierre}. Consider the following, real-life, example taken from the corpus of the \textit{Régie du logement du Québec} (see Section~\ref{sec:the_original_dataset})

\begin{center}
	\begin{tabular}{m{.8\linewidth}}
	\begin{example}
	\label{ex:landlord}
		``The landlord demands the termination of the lease and the eviction of the tenant, the recovery of the rent (1,080 \$) as well as the rent owed at the time of the audience, plus the provisional execution of the decision despite de outcome. This is concerning a lease from December 1 2016 to June 30 2018 at a monthly rent of 545 \$, to be payed the first of each month. The proof shows that the tenant owes 1,620 \$, this being the rent corresponding to the months of February, March, and April 2017, plus 9 \$ corresponding the costs of notification as stipulated by the Rules. The tenant is more than three months late in their rent payments, thus the termination of the lease is justified by applying Article 1,971 of the Civil Code of Québec. The lease is however not terminated if the owed rent, the interests, and the costs, are payed before the decision, conforming to the dispositions laid down by Article 1,883 of the Civil Code of Québec. The injury caused to the landlord justifies the provisional execution of the decision, as foreseen by Article 82.1 of the Law on the \textit{Régie du logement}. Can the tenant's lease be terminated? Can it be terminated immediately, as the landlord requests? ''
	\end{example}
	\end{tabular}
\end{center}

Because of the language, the complexity of the case, and the length of the text, construing the facts of the case is not as straightforward as in Example~\ref{ex:pierre}. Moreover, this is a relatively short and simple, real-life case; longer cases can span thousands of words and describe much more complex situations.

\subsection{The sentence} % (fold)
\label{sub:the_sentence}

% subsection the_sentence (end)
The basic unit in the text that contains a fact is the sentence; this can be observed in both Examples~\ref{ex:pierre} and \ref{ex:landlord}. In this context, a sentence is understood to be a self-contained grammatical unit usually delimited by a period, though sometimes other punctuation markers, such as semicolons or dashes, might be used. While a sentence may contain more than one fact, it is not likely that a single fact spans multiple sentences. Thus, a part of the challenge of extracting facts from a case is to determine whether a given sentence contains one or several facts or not.

As mentioned previously, this is challenging for the non-expert, because of the complexities of style, vocabulary, domain knowledge, and real-world knowledge. As a real-world example of the challenge of labelling sentences as either Facts or non-Facts, consider the samples in Figure~\ref{fig:classification_example}. 

\begin{figure}
	\centering
	\input{chapters/ch1/pictures/clf_example}
	\caption{Example of sentence classification as either stating a case Fact or non-Fact. (English translations provided by the authors. For privacy reasons, proper names have been redacted.)}
	\label{fig:classification_example}
\end{figure}
% subsection what_is_a_fact_ (end)

Figure~\ref{fig:classification_example} shows a segment of a real-like example taken from the corpus of the \textit{Régie du logement} (see Section~\ref{sec:the_original_dataset}), where four sentences were labelled as either containing \textit{Facts} or \textit{non-Facts} by the author of the ruling (in this case, the judge). We can see the difficulty in classifying each sentence because of the use of technical words (eg \textit{mandate}, \textit{emanates}), domain knowledge (the citation of specific articles of law), and real-world knowledge (the fact that the judge is aware that the <ORG> entities are distinct).

% To this end, we use a Deep Learning (DL) model to perform the sentence classification task and propose a method of text segmentation based on maximising the length and relevance of the sequence of sentences containing facts\footnote{The source code is available \href{https://github.com/CLaC-Lab/JusticeBot}{here}.}.

\section{IRAC and the base assumption} % (fold)
\label{sec:base_assumption}

The IRAC standard model (“Issue-Rule-Application-Conclusion”) is a syllogistic formula for organising legal analysis \parencite{beazley2018practical}. It is a standard of teaching in many first-year law introductory courses and it has many variants across the different publications and schools of thought (cf \parencite{beazley2018practical}, \parencite{felsenburg2010beginning} and \parencite{flanigan2019lawyer}). Although its use is more widespread in academic circles \parencite{garner2009garner}, it constitutes a good starting point on the writing of legal documents by providing a theoretical framework with which to craft legal arguments. In its most basic form, the IRAC standard model follows the structure illustrated in Example~\ref{ex:luthien1}:

\begin{center}
\begin{tabular}{m{.8\linewidth}}
	\begin{example}
	\label{ex:luthien1}
		$\quad$
		\begin{center}
		\begin{tabular}{ll}
			\textit{Issue:} & Is Lúthien immortal? \\
			\textit{Rule:} & Men are mortal, elves are immortal \\
			\textit{Application:} & Lúthien is an elf-maiden \\
			\textit{Conclusion:} & Lúthien is immortal\\
		\end{tabular}
		\end{center}
	\end{example}
\end{tabular}
\end{center}

The legal document explaining the issue and resolution of this matter would follow the four stages of the IRAC framework. However, as previously mentioned, IRAC has many variants, which consist of adding other components in order to refine the relation between the rule (\textit{Men are mortal, elves are immortal}) and the application (\textit{Lúthien is an elf-maiden}). For example, if we introduce a new component, the \textit{Explanation} \parencite[p. 94]{beazley2018practical}, into de framework, we can refine the original syllogism as follows:

% The legal document explaining the issue and resolution of this matter would follow the four stages of IRAC: \textit{Issue, Rule, Application} and \textit{Conclusion}. The familiar structure of the syllogism in Example~\ref{ex:luthien1} is a good starting point, but it is not enough to convey the relation between the rule (\textit{Elves are immortal}) and the application (\textit{Lúthien is an elf-maiden}); an additional \textit{Explanation} point should be added in order to clarify why the rule is relevant to the application. Thus, it is possible to modify the original IRAC framework in order to introduce an \textit{Explanation}:

\begin{center}
\begin{tabular}{m{.8\linewidth}}
	\begin{example}
		\label{ex:luthien2}
		$\quad$
		\begin{center}
		\begin{tabular}{ll}
		\textit{Issue:} & Is Lúthien immortal? \\
		\textit{Rule:} & Men are mortal, Elves are immortal \\
		\textit{Explanation:} & Elf-maidens are Elves \\
		\textit{Application:} & Lúthien is an Elf-maiden \\
		\textit{Conclusion:} & Lúthien is immortal\\
		\end{tabular}
		\end{center}
	\end{example}
\end{tabular}
\end{center}

The \textit{Rule} and the \textit{Explanation} in both Examples~\ref{ex:luthien1} and \ref{ex:luthien2} correspond to facts which, when applied, permits the drawing of a conclusion. However, the application and the associated explanation are not ontologically necessary to the other components: a rule may exist whether there is a particular application of it or not, just as Men would remain mortal and Elves would remain immortal even if there were no Elf-maiden named Lúthien from whose mortality a conclusion could be drawn. In the context of this thesis, the \textit{Rules}, \textit{Explanations} and \textit{Applications} in Examples~\ref{ex:luthien1} and \ref{ex:luthien2} correspond to the real-life facts found in each of the documents in our corpus (see Chapter~\ref{ch:dataset}), as illustrated in Example~\ref{ex:luthien3}.


\begin{center}
\begin{tabular}{m{.8\linewidth}}
	\begin{example}
		\label{ex:luthien3}
		$\quad$
		\begin{center}
		\begin{tabular}{ll}
		\textit{non-Fact:} & Is Lúthien immortal? \\
		\textit{Fact:} & Men are mortal, Elves are immortal \\
		\textit{Fact:} & Elf-maidens are Elves \\
		\textit{Fact:} & Lúthien is an Elf-maiden \\
		\textit{non-Fact:} & Lúthien is immortal\\
		\end{tabular}
		\end{center}
	\end{example}
\end{tabular}
\end{center}

According to the IRAC framework and its variants, these should exist as semantically independent and distinguishable units of discourse, ie sentences. Moreover, if the framework, and its variants, are as widespread as the literature suggests, these should occupy the same place, relative to the body of text, in each legal document in our corpus, namely, before any sort of \textit{Conclusion} is presented. Thus, Figure~\ref{assumption} presents the basic assumption of this thesis.

\begin{figure}[ht!]
\begin{ftheo}
	The text of a decision will follow the structural convention of first stating the facts and then performing an analysis based on them.		
\end{ftheo}
	\caption{The basic assumption of this thesis}
	\label{assumption}
\end{figure}

The assumption that the text of a decision will follow the structural convention of first stating the facts and then performing an analysis based on them is intuitive, since a case analysis, that is, its explanation, application and conclusion, is built from its facts, which gives way to the best decisions. Hence, the sentences of such decisions should follow this discourse structure. 

% Because a high recall of fact extraction is more important in our project, we propose a method based on maximising the length of the Facts segment while simultaneously maximising its “purity”, which we define as the ratio of the number of fact-bearing sentences with respect to the length of the segment.

\section{The JusticeBot project} % (fold)
\label{sec:the_justicebot_project}
The JusticeBot project \parencite{westermann2019using} is an initiative by the CyberJustice Lab at the Université de Montréal that aims to provide a gateway to law and jurisprudence for lay people through a chatbot where users can seek remedies to terminate their lease because of landlord-tenant disputes. As part of the extrinsic evaluation of the system as a whole, a \textit{challenge task} was conceived whereby participants would be asked to build predictive systems that would be required to answer a number of questions related to the outcome of a given case. For example, given a document, or the relevant case facts of a documents, participants would have to predict whether the judge ruled in favour of the tenant, the amount of the judge order as compensation to either the plaintiff or the defendant, length of a sentence, etc. The challenge tasks are in this manner akin to the shared tasks of SensEval \parencite{edmonds2002senseval} and SemEval \parencite{strapparava2007semeval}. Prior to the deployment of the challenge, a system capable of extracting case-related facts in order to build a training corpus for the task was required.
% section the_justicebot_project (end)

% section base_assumption (end)
\section{Goal of this thesis} % (fold)
\label{sec:problem_description}

Starting from the assumption in Figure~\ref{assumption}, this thesis proposes a set of approaches to identify and segment \textit{Facts} in texts of judicial rulings. 

% The extraction of \textit{Facts} from a ruling is performed by classifying each sentence in the text as either belonging to the \textit{Facts} section of the document or not; this is followed by the identification of the boundary between the segment that holds the \textit{Facts} and the segment that holds the \textit{non-Facts}.

Specifically, given the text of a judicial decision, using the principles of IRAC and its variants, and assuming the document follows the convention of listing all relevant case facts before the analysis and conclusion, this thesis seeks to extract the case relevant facts by means of neural approaches for text segmentation in the legal domain. We approach the problem using two different methodologies:
\begin{itemize}
 	\item By performing individual Binary Sentence Classification to determine which segment of the text contains the \textit{Facts} and which contains the \textit{non-Facts}, where the outcome of each sentence is independent of the outcome of all others (see Chapter~\ref{ch:segmentation_sentence_clf}).
 	\item By performing the segmentation using a recurrent architecture to classify each sentence in the document as either \textit{Facts} or \textit{non-Facts}, where the outcome of each classification is conditioned by the context in which the sentence is found (see Chapter~\ref{ch:segmentation_recurrent}).
 \end{itemize}

We evaluate both methodologies using intrinsic evaluation metrics (accuracy, precision, recall, and F$_1$) and an extrinsic metric consisting of counting the number of documents whose segmentation is off by a varying number of sentences.
% section problem_description (end)

\section{Contributions} % (fold)
\label{sec:contributions}
This thesis presents the following contributions:
\begin{itemize}
	\item It introduces neural approach to a relevant task in the intersection of Natural Language Processing and Law: the segmentation of legal texts. We present two novel methodologies based on the principles Sentence Classification, Contextual and non-Contextual Embeddings applied to the task of extracting factual information from legal cases concerning tenant-landlord disputes.
	\item It builds on and contributes to the JusticeBot, a project developed by the CyberJustice Laboratory with the aim of simplifying access to legal information for the public. At the time of writing, JusticeBot is focused on facilitating the access to information concerning landlord-tenant disputes. In particular, this thesis provides a methodology by which the training and testing datasets of the challenge task can be built (see Section~\ref{sec:the_justicebot_project}).
\end{itemize}
% section contributions (end)

\section{Thesis structure} % (fold)
\label{sec:thesis_structure}
This chapter has briefly described the nature and challenges of extracting case-relevant facts from judicial decisions, and the assumption on which we base the Text Segmentation task. The rest of this thesis is structured as follows: Chapter~\ref{ch:related_work} explores the work done in the fields of Law and AI in terms of the application of Natural Language Processing, with special emphasis on neural methods; Chapter~\ref{ch:dataset} describes the nature, acquisition, and processing of the corpus we use to train and evaluate the models on which our approaches are based; Chapter~\ref{ch:segmentation_sentence_clf} describes in detail the principle, training, and evaluation of the first approach, the Sentence Classifier method; Chapter~\ref{ch:segmentation_recurrent} describes in detail the principle, training, and evaluation of the second approach, the Recurrent Architecture; finally, Chapter~\ref{ch:conclusion} provides a summary of the thesis and proposes improvements and future work.
% section thesis_structure (end)