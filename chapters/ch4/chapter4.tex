\chapter{Text segmentation using sentence classification}
\label{ch:segmentation_sentence_clf}

This chapter will describe our first approach to perform text segmentation in order to extract Facts from the documents of our corpus. The approach takes as input a sequence of sentences and produces a binary sequence, where each sentence is classified either as  \textit{Facts} sentence or a \textit{non-Facts} sentence. This binary classification approach would ideally result in a representation where all the sentences belonging to the facts will appear as a contiguous sequence of 1's and all other sentences will appear as a contiguous sequence of 0's. The evaluation of this approach (see Sections~\ref{sec:evaluation_of_the_sentence_classifier} and \ref{sec:evaluation_of_the_segmentation_task_sentence}) show that this method, while yielding a performance that surpasses that of the baseline, can be further improved by taking into account the context in which each sentence is found.

\section{Sentence classification} % (fold)
\label{sec:sentence_classification}

Our first approach at Fact segmentation is based on the binary classification of each sentence, where only the content of the of the sentence to classify is taken into account.

\subsection{Model architecture} % (fold)
\label{sub:model_architecture}

To perform the sentence classification, each sentence in the text is converted to a tensor using a vocabulary and a list of indices built from the pre-trained, non-domain-specific, French word embeddings model of \textcite{baroni2009wacky}. The tensor is then fed into a neural model that outputs a binary classification, indicating whether the sentence belongs to the facts or the non-facts. The architecture of the sentence classification is shown in Figure~\ref{fig:model_architecture}.


\begin{figure}
	\centering
	\subfloat[\linewidth][Recurrent part of the classifier: A tokenised sentence, represented by a sequence $x_1, x_2, \hdots ,x_T$, is passed through an embedding layer, whose outputs are passed into a stack of GRU layers, producing a context vector $h_T^{(R)}$. \label{fig:recurrent}]{
		\begin{tabular}{p{.7\linewidth}}
		\centering
		\input{chapters/ch4/pictures/recurrent}
		\end{tabular}
		}

	\subfloat[\linewidth][CNN part of the classifier. The input is a tensor of size $1\times T \times k$, where $T$ is the sequence length, and $k$ is the size of the word vector. The output feature maps are passed through a 1-D Max Pool layer that produces an output vector $h_T^{(C)}$. \label{fig:convolutional}]{
		\begin{tabular}{p{.7\linewidth}}
		\centering
		\input{chapters/ch4/pictures/conv}
		\end{tabular}
	}

	\subfloat[Concatenation of the context vector $h_T^{(R)}$ and the output vector $h_T^{(C)}$. The resulting tensor is passed through an affine layer with a softmax activation function to produce the probability of the sentence being Facts. \label{fig:concatenation}]{
	\begin{tabular}{p{.7\linewidth}}
		\centering
		\input{chapters/ch4/pictures/concat}
	\end{tabular}
	}
\caption{Architecture of the sentence classifier.}
\label{fig:model_architecture}
\end{figure}

The model is a combination of recurrent and convolutional architectures.  Overall, a sequence of sentences are fed through the model, and a binary sequence of $L$ entries is produced, where the value 1 represents sentences that belong to the facts and 0 represents those belonging to the non-facts. In the recurrent architecture (see Figure~\ref{fig:recurrent}), a tokenised sentence of length $T$, represented by a sequence of words $x_1, x_2, \hdots ,x_T$, is passed through an embedding layer, whose outputs are passed into a bidirectional stack of Gated Recurrent Unit (GRU) layers \parencite{sutskever2014sequence}, producing a context vector $h_T^{(R)}$. In the convolutional part (see Figure~\ref{fig:convolutional}), the input is a tensor of size $n\times T \times k$, where $n$ is the batch size and $k$ is the size of the word vector (ie the embedding size). The output feature maps are passed through a 1-D Max Pool layer that produces an output vector $h_T^{(C)}$. Finally, $h_T^{(R)}$ and $h_T^{(C)}$ are concatenated (see Figure~\ref{fig:concatenation}), and the resulting vector is passed through an affine layer with a softmax activation function to produce the probability of the sentence being a \textit{Fact}.

We used both the original dataset and the augmented datasets detailed in Section~\ref{sec:data_augmentation}. We used 75\% of the dataset for training, 10\% validation, and the remaining 15\% for testing. The model's hyperparameters are shown in Table~\ref{tab:hyperparams}.	

\begin{table}[ht!]
	\centering
	\begin{tabular}{rrrrrr}
	\toprule
\multicolumn{2}{r}{\textbf{General  hparams}} &  & \multicolumn{2}{r}{\textbf{GRU hparams}} \\
Batch size ($n$)         & 512                &  & Num layers                & 3            \\
Embedding size ($k$)       & 200                &  & Dropout                   & 0.5          \\
Gradient clipping        & 0.2                &  & Hidden size               & 512          \\
Weight decay             & 1e-4               &  & Bidirectional             & Yes          \\
Learning rate            & 1e-\{3,4,5\}       &  &  \\
Optimiser                & ADAM               &  & \multicolumn{2}{r}{\textbf{CNN hparams}}                          &           \\
                         &                    &  & Dropout                   & 0.2          \\
                         &                    &  & Output channels           & 100            \\
                         &                    &  & Kernel size               & 3             \\
\bottomrule\\
\end{tabular}
\caption{Model hyperparameters of the sentence classifier}
\label{tab:hyperparams}
\end{table}

% subsection model_architecture (end)

\section{Evaluation of the sentence classifier} % (fold)
\label{sec:evaluation_of_the_sentence_classifier}
We evaluated the sentence classifier using the test datasets of both the original corpus and the augmented corpora (15\% of each). As the method of intrinsic evaluation, we used accuracy, precision, recall and the F$_1$ score for the class \textit{Fact}, using the batch size as sample size. Results are shown in Table~\ref{tab:sentence_clf_results}.

\begin{table}[ht!]
\centering
	\begin{tabular}{rrrrr}
		& Accuracy & Precision & Recall & F$_1$ \\
		\toprule
		Original dataset & 0.7887 & 0.7770 & 0.7569 & 0.7668 \\
		Augmented dataset by 2 & 0.7906 & 0.7797 & 0.7579 & 0.7686 \\
		Augmented dataset by 3 & 0.7886	& 0.7773 & 0.7562 &	0.7668 \\
		\bottomrule\\
	\end{tabular}
	\caption{Evaluation metrics of the sentence classifier using both the original and the augmented datasets}
	\label{tab:sentence_clf_results}
\end{table}
% section evaluation_of_the_sentence_classifier (end)

\section{Text segmentation} % (fold)
\label{sec:sentence_clf_segmentation}
Once each sentence is classified as fact (1) or non-fact (0), the next step is to optimally divide the sequence into two substrings, each representing the \textit{Facts} and \textit{non-Facts} segments of the ruling. Figure~\ref{fig:binary_segmentation} illustrates this. To perform the segmentation, we establish the following propositions:

\begin{itemize}
	\item Let $L$ represent the number of sentences document and let $L_f$ represent the number of sentences in its Facts segment.
	\item Let $n_f$ represent the number of Facts sentences found in $L_f$ (such that $n_p\le L_f$).
	\item Define $p_f = \frac{n_f}{L_f}$ as the \textit{purity} of $L_f$.
\end{itemize}

\begin{figure}
	\centering
	\input{chapters/ch4/pictures/segmentation}
	\caption[Visual representation of the segmentation of the binary string.]{Visual representation of the segmentation of the binary string where 1 refers to a sentence classified as \textit{fact} and 0 as \textit{non-fact}. In this example, $L_f = 13$, $L=24$ and $n_f=12$}.
	\label{fig:binary_segmentation}
\end{figure}

We wish to maximise both $L_f$ and $p_f$; maximising $L_f$ is equivalent to maximising the recall of facts in the segmentation, and maximising $p_f$ is done to ensure the segmentation corresponds as closely as possible to the gold standard, in keeping with our central assumption (see Figure~\ref{assumption}). Maximising both variables at the same time can be described as the following optimisation problem:

\begin{equation} \label{eq:loss_function}
	\begin{aligned}[t]
		 \max J(L_f) &=  \max \left(\alpha L_f + \beta p_f\right)\\
	\end{aligned}
\end{equation}

where $J$ is a loss function of $L_f$, and $\alpha,\beta \in \mathbb{R}$ are arbitrary weights representing the importance of each term. We can rewrite and differentiate Equation~\ref{eq:loss_function} to find an expression that optimises $L_f$:
	\begin{align*}
		\min -J(L_f) &= \min -\left( \alpha L_f + \beta \frac{n_f}{L_f} \right) \\
		-J'(L_f) &= -\alpha + \beta \frac{n_f}{L_f^2} \\
		0 &= -\alpha L^2_f + \beta n_f \\
		L^2 &= \frac{\beta}{\alpha} n_f \\
		L_f &= \frac{\beta}{\alpha}p_f \numberthis \label{eq:l_p_linear}
	\end{align*}

Equation~\ref{eq:l_p_linear} indicates that there is a linear relation between the purity ($p_f$) of a substring and its length ($L_f$). Rather than conducting a large number of experiments to empirically determine optimal values for $\alpha$ and $\beta$, we turned to a simpler strategy: given the linear relation between length and purity, for all possible substrings of length $L_{f_i}$ in the original string, we select the one that maximises $p_f$. Since any substring, including one with a single member, comprised exclusively of 1's will have a trivial purity $p_{f_i}=1$, we select $L_{f_i}$ that maximises $p_f$ such that $p_f \neq 1$.

By considering the problem of finding the optimal segmentation point as extracting the substring with the highest purity from a binary representation of the document, our model can compute a splitting index, $l$, which can be empirically weighted by a factor, $\gamma \propto \frac{\beta}{\alpha}$ in order to favour either shorter or longer substrings. Shorter substrings will be purer and will contain few instances of non-Facts, while longer substrings will favour the recall of sentences containing facts. Hence, we can compute the weighted splitting index $L_\gamma$ as $L_\gamma = \gamma l$

\subsection{Evaluation of the segmentation task} % (fold)
\label{sec:evaluation_of_the_segmentation_task_sentence}
As a method of extrinsic evaluation, we test the text segmentation over the test fraction of annotated cases in the original corpus (660 documents) using the gold standard described in Section~\ref{sec:the_gold_standard} and using three classifiers trained respectively on the test set, and the test set augmented by factors of 2 and 3 using the methodology described in Section~\ref{sec:data_augmentation}. We made sure that no augmented instances were included in the test set. 

Given a value of $\gamma$, we compute a corresponding splitting index $l$ for each document, and split the text according to the weighted splitting index $L_\gamma$. We then count the number of sentences by which the resulting text is off compared to the gold standard of number of sentences in the Facts section. Results are shown in Table~\ref{tab:sentence_clf_gamma} and Figure~\ref{fig:sentence_clf_gamma} for the original dataset, and in Table~\ref{tab:sentence_clf_gamma_augm} and Figure~\ref{fig:sentence_clf_gamma_augm} for the augmented datasets.
% section evaluation_of_the_segmentation_task_sentence (end)

\begin{table}
	\centering
	\input{chapters/ch4/pictures/sentence_clf_gamma_table}
	\caption{Sentence offset results for the different values of $\gamma$ using the original dataset for training and testing.}
	\label{tab:sentence_clf_gamma}
\end{table}

\begin{table}
	\centering
	\input{chapters/ch4/pictures/sentence_clf_gamma_table_augm}
	\caption{Sentence offset results for the different values of $\gamma$ using the doubled dataset for training and testing.}
	\label{tab:sentence_clf_gamma_augm}
\end{table}

\begin{table}
	\centering
	\input{chapters/ch4/pictures/sentence_clf_gamma_table_augm_2}
	\caption{Sentence offset results for the different values of $\gamma$ using tripled}
\end{table}

Using the sentence classifier trained on the original dataset, Table~\ref{tab:sentence_clf_gamma} and Figure~\ref{fig:sentence_clf_gamma} show that, with the original test, for $\gamma=1$, we obtain 39 documents ($15+11+13=39$ or 6\% of the dataset) that fall within a single sentence of difference with respect to the gold standard, and 11 documents (1.7\%) being segmented exactly where the gold standard indicates. Nevertheless, 529 documents (80\%) are split using an underestimated index and fall short of the target, being more than 4 sentences away from the gold standard. Increasing the value of $\gamma$ favours the recall of sentences annotated as Facts, but the percentage of documents whose segmentation falls short by more than 4 sentences does not decrease as quickly as the percentage of overestimated segmentations; for $\gamma = 1.4$, the number of underestimated segmentations by a margin greater than 4 is still 520 (78\%).

When using the classifiers trained on the augmented datasets, little to no improvement is observed. Table~\ref{tab:sentence_clf_gamma_augm} and Figure~\ref{fig:sentence_clf_gamma_augm} show that, with the dataset augmented by a factor of 2, for $\gamma=1$, we get 32 documents ($15+11+6$ or 4.8\% of the dataset) that fall within a single sentence of difference with respect to the gold standard. However, 537 documents (81\%) are still more than 4 sentences away from the expected splitting point. Once again, increasing the value of $\gamma$ has the same effect as increasing the recall of sentences annotated as Facts, and for $\gamma=1.4$ we see now that 524 documents (79\%) lie more than 4 sentences away from the predicted splitting index.

For the different values of $\gamma$, the distributions of offsets (Figures~\ref{fig:sentence_clf_gamma} and~\ref{fig:sentence_clf_gamma_augm}) present a large number of underestimated splitting indices in all cases. This fact seemingly stands in contrast to our base assumption (Section~\ref{sec:base_assumption}), that facts should always give way to analyses: the gold standard expects us to find many more facts after the predicted splitting index, weighted or otherwise, which suggests that some cases either contain an imbalance of facts and analyses or contain facts and analysis interspersed with each other on a larger scale than expected.

Moreover, the lack of improvement in performance of the sentence classifier using the augmented dataset compared to the original dataset (see Table~\ref{tab:sentence_clf_results}) influences the value of the performance of our splitting heuristic, but it does not significantly alter the trend observed in the charts (see Figures~\ref{fig:sentence_clf_gamma} and \ref{fig:sentence_clf_gamma_augm}). We speculate that larger amounts of data would not significantly alter the trend, and that only an increase in performance would avoid the considerable percentage of underestimated documents.

\begin{figure}
	\centering
	\subfloat[0.5\linewidth][$\gamma=0.6$]{
	\scalebox{0.8}{\input{chapters/ch4/pictures/gamma06}}
	}
	\subfloat[0.5\linewidth][$\gamma=0.8$]{
	\scalebox{0.8}{\input{chapters/ch4/pictures/gamma08}}
	}

	\subfloat[0.5\linewidth][$\gamma=1$]{
	\scalebox{0.8}{\input{chapters/ch4/pictures/gamma10}}
	}
	\subfloat[0.5\linewidth][$\gamma=1.2$]{
	\scalebox{0.8}{\input{chapters/ch4/pictures/gamma12}}
	}

	\subfloat[0.5\linewidth][$\gamma=1.4$]{
	\scalebox{0.8}{\input{chapters/ch4/pictures/gamma14}}
	}
	\caption{Sentence offset distribution for the different values of $\gamma$ using the original dataset}
	\label{fig:sentence_clf_gamma}
\end{figure}

\begin{figure}
	\centering
	\subfloat[0.5\linewidth][$\gamma=0.6$]{
	\scalebox{0.8}{\input{chapters/ch4/pictures/gamma06_augm}}
	}
	\subfloat[0.5\linewidth][$\gamma=0.8$]{
	\scalebox{0.8}{\input{chapters/ch4/pictures/gamma08_augm}}
	}

	\subfloat[0.5\linewidth][$\gamma=1$]{
	\scalebox{0.8}{\input{chapters/ch4/pictures/gamma10_augm}}
	}
	\subfloat[0.5\linewidth][$\gamma=1.2$]{
	\scalebox{0.8}{\input{chapters/ch4/pictures/gamma12_augm}}
	}

	\subfloat[0.5\linewidth][$\gamma=1.4$]{
	\scalebox{0.8}{\input{chapters/ch4/pictures/gamma14_augm}}
	}
	\caption{Sentence offset distribution for the different values of $\gamma$ using the doubled augmented dataset}
	\label{fig:sentence_clf_gamma_augm}
\end{figure}

\begin{figure}
	\centering
	\subfloat[0.5\linewidth][$\gamma=0.6$]{
	\scalebox{0.8}{\input{chapters/ch4/pictures/gamma06_augm_2}}
	}
	\subfloat[0.5\linewidth][$\gamma=0.8$]{
	\scalebox{0.8}{\input{chapters/ch4/pictures/gamma08_augm_2}}
	}

	\subfloat[0.5\linewidth][$\gamma=1$]{
	\scalebox{0.8}{\input{chapters/ch4/pictures/gamma10_augm_2}}
	}
	\subfloat[0.5\linewidth][$\gamma=1.2$]{
	\scalebox{0.8}{\input{chapters/ch4/pictures/gamma12_augm_2}}
	}

	\subfloat[0.5\linewidth][$\gamma=1.4$]{
	\scalebox{0.8}{\input{chapters/ch4/pictures/gamma14_augm}}
	}
	\caption{Sentence offset distribution for the different values of $\gamma$ using the tripled augmented dataset}
	\label{fig:sentence_clf_gamma_augm_2}
\end{figure}



\section{Chapter summary} % (fold)
\label{sec:summary_4}
In this chapter we have described our first approach to Fact segmentation based on the classification of each sentence as either \textit{Facts} or \textit{non-Facts} and segmenting the document according to the greatest number of facts in the longest segment of text. We have shown that the independent classification of sentences as either Fact of non-Fact, and the subsequent segmentation of the resulting binary string, yields a large percentage of documents whose segmentation point is underestimated.

In the next chapter we will describe a different approach, namely, one where the classification of sentences as \textit{Facts} or \textit{non-Facts} is not independent with one another but actually conditioned by the context in which they are found.
% section summary_4 (end)