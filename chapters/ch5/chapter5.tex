\chapter{Text segmentation using recurrent architectures}
\label{ch:segmentation_recurrent}

The previous chapter described how to segment the text of a document into \textit{Facts} and \textit{non-Facts} by independently classifying each sentence. This idea relies on the assumption that a sentence will be classified into either category regardless of the context in which it is found. This is a questionable notion at best because of the expected discourse structure of legal documents (see Section~\ref{sec:base_assumption}); according to the IRAC paradigm (see Section~\ref{sec:base_assumption}, sentences will tend to keep congruent semantics, and the probability of finding a sentence stating a fact amongst sentences discussing an application of the facts is small.

In order to take into account both the content and the context of a given sentence, we turn to a different architecture as the basis of our second approach: a bidirectional recurrent model in which each element in the time series is a function of all others. We represent each document in our dataset as a sequence of sentences, and each sentence as a dense vector, using camemBERT \parencite{2019arXiv191103894M} to create contextual sentence embeddings. We then experiment with two neural architectures: a recurrent model using an LSTM/GRU recurrent network \parencite{hochreiter1997long,cho2014learning} and a sequence-to-sequence (seq2seq) attention model \parencite{bahdanau2014neural}.

Section~\ref{sec:memory_constraints} will describe the use of Contextual Sentence Embeddings in our models and the memory constraints we had to overcome in order to add them to our architecture. Section~\ref{sec:lstm_and_gru} will describe the specific architecture of the recurrent models we used to carry out the classification task: the LSTM/GRU networks and the Attention model; Section~\ref{sec:evaluation_of_the_recurrent_architectures} will deal with the intrinsic evaluation of these models. Finally, Section~\ref{sec:evaluation_of_the_segmentation_task_recurrent} will present and discuss the results of the evaluation of the segmentation task using recurrent architectures.

\section{Sentence embeddings} % (fold)
\label{sec:memory_constraints}
During the course of our experiments, it became apparent that the memory constraints of using a large BERT-like model such as camemBERT would make it impossible to include it as an additional module to fine-tune; because of the average number of sentences per document in our corpus (see Table~\ref{tab:dataset_stats}), creating the contextual sentence embeddings as part of the architecture's pipeline exceeded our resources for the vast majority of instances in our training set. Therefore, we had to resort to using camemBERT to pre-train the contextual sentence embeddings without the possibility of fine-tuning the model to our specific task.
% section memory_constraints (end)

\section{Architectures of the LSTM/GRU networks and the Attention model} % (fold)
\label{sec:lstm_and_gru}

We experimented with two recurrent architectures. The first architecture, shown in Figure~\ref{fig:lstm_gru} is based on LSTM and GRU recurrent networks. At each time step, a sentence embedding is fed into the cell and a binary output is produced as a function of the hidden states from both the previous and the following time steps, as well as from the input. The recurrent network is bidirectional, meaning that each output contains information from both previous and future representations of the document. This was meant to add contextual information to the final binary output, which results in a sentence's classification being a function of both its own representation and the context in which it is found.

\begin{figure}[ht!]
	\centering
	\scalebox{0.9}{
	\input{chapters/ch5/pictures/lstm_gru}	
	}
	\caption[The bidirectional LSTM/GRU recurrent network.]{The bidirectional LSTM/GRU recurrent network. At each time step, an output is produced as a function of the previous and following hidden states, and the sentence input.}
	\label{fig:lstm_gru}
\end{figure}

The second architecture, shown in Figures~\ref{fig:attn_encoder} and \ref{fig:attn_decoder}, is a seq2seq model using an Attention mechanism (see Section~\ref{ssub:attention}). The implementation follows the original mechanism introduced by \textcite{bahdanau2014neural}, with inspiration for the code implementation taken from \textcite{lin2017structured}: we create the annotations of the input sentence $\mathbf{H} = [\mathbf{h}_1 \mathbf{h}_2 \hdots \mathbf{h}_T]$ using the concatenation of the hidden state at both directions at each time step, $\mathbf{h}_i = \overrightarrow{\mathbf{h}_i} ; \overleftarrow{\mathbf{h}_i}$, as shown in Figure~\ref{fig:attn_encoder}. Each annotation is a representation that summarises the current input token along with all other tokens before and after. These annotations will be afterwards weighted at each time step during the decoding phase in order to tell the Decoder which tokens to pay attention to produce the corresponding output.

\begin{figure}
	\centering
	\input{chapters/ch5/pictures/attn}
	\caption[The Attention Encoder.]{The Attention Encoder. The hidden states at each time step are concatenated to create the annotation of the corresponding input token. These annotations are representations of each of the input tokens, containing information from all other tokens in the input sequence.}
	\label{fig:attn_encoder}
\end{figure}

After the annotations are created, the Decoder, shown in Figure~\ref{fig:attn_decoder}, takes a randomly initialised hidden state and iteratively outputs a binary sequence, producing each token as a function of the previous hidden state and the weighted annotations. The \textit{attention weights} assigned to each annotations are produced by a feedforward network that is jointly trained with all other components.

\begin{figure}
	\centering
	\input{chapters/ch5/pictures/attn_dec}
	\caption[The Attention Decoder.]{The Attention Decoder. At each time step, the Decoder, implemented as a GRU cell, receives the hidden state from the previous step, $\mathbf{h}_{i-1}$, and the annotations of the input sequence, $\mathbf{H}$. It assigns the corresponding weight to each annotation and uses the result to determine the tokens of the input sequence that best inform the output $\mathbf{h}_i$.}
	\label{fig:attn_decoder}
\end{figure}

\begin{table}
	\centering
	\begin{tabular}{rr}
		\textbf{Hyperparameter} & \textbf{Value} \\
		\toprule
        Batch size & 1 \\
        Nb epochs & 25 \\
        Learning rate & 1e-{3, 4, 5} \\
        Weight decay & 1e-4 \\
        Gradient clip & 0.2 \\
        Learning rate step & [10 \\ 13] \\
        Learning rate gamma & 0.1 \\
        Optimiser & ADAM \\
        Bidirectional & Yes \\
        Input size & 768 \\
		\bottomrule\\
	\end{tabular}
	\caption{Model hyperparameters of the recurrent architectures}
\end{table}

\section{Evaluation of the recurrent architectures} % (fold)
\label{sec:evaluation_of_the_recurrent_architectures}

To evaluate the recurrent models, we proceed with a similar approach as with the Sentence Classifier method (see Section~\ref{sec:evaluation_of_the_sentence_classifier}). We compare the output of each instance produced by the recurrent models with the gold standard described in Section~\ref{sec:the_gold_standard}. We trained the models on both the original dataset and the augmented dataset (see Section~\ref{sec:data_augmentation}) splitting both into training, validation, and testing fractions (75:10:15), using the same split as used for our first approach. As in the case of our first approach, we made sure the testing fraction contained no augmented instances.

A key difference between the evaluation of this approach and the evaluation of our first approach (see Section~\ref{sec:evaluation_of_the_sentence_classifier}) is that we do not use the batch size as sample size to compute the metrics of Accuracy, Precision, Recall and F$_1$. Recall that, unlike in the previous approach, the result of a single training iteration is not the binary classification of a single sentence but the output of a binary sequence representing all the sentences in the document at once. Moreover, because of memory constraints, a training iteration using contextual embeddings could only be allocated a batch size of 1. Refer to Figure~\ref{fig:why_evaluation_different} for a visualisation.

\begin{figure}
	\centering
	\subfloat[\linewidth][First approach]{
	\input{chapters/ch5/pictures/why_eval_diff_1} \label{fig:approach1}
	}
	\subfloat[\linewidth][Second approach]{
	\input{chapters/ch5/pictures/why_eval_diff_2} \label{fig:approach2}
	}
	\caption[Visual representation of the sample size used to compute performance metrics.]{Visual representation of the sample size used to compute performance metrics. In the first approach the batch size $n$ is used as sample size because each sentence $s_i$ is independently classified, regardless of the context in which it is found. In the second approach, the length $T_i$ of each document $d_i$ is used as sample size because each classified sentence $y_i$ is conditioned on all other sentences in the document and each document .}
	\label{fig:why_evaluation_different}
\end{figure}

Each entry in the corpus is annotated with a corresponding binary sequence that represents the segmentation of Facts and non-Facts (see Section~\ref{sec:the_gold_standard}); we use these binary sequences to compute the evaluation metrics in our second approach. Results are shown in Table~\ref{tab:recurrent_models_results}.

\begin{table}[ht!]
	\centering
	\begin{tabular}{lcccc}
		\textbf{Architecture} & \textbf{Accuracy} & \textbf{Precision} & \textbf{Recall} & \textbf{F$_1$} \\
		\toprule
		LSTM (original) & 0.9865 & 0.9874 & 0.9884 & 0.9879 \\
		LSTM (augmented by 2) & 0.9638 & 0.9690 & 0.9718 & 0.9704 \\ 
		LSTM (augmented by 3) & 0.9661 & 0.9701 & 0.9762 & 0.9731 \\ 
		\hline
		GRU (original) & 0.9888 & 0.9883 & 0.9918 & 0.9900 \\
		GRU (augmented by 2) & 0.9611 & 0.9645 & 0.9719 & 0.9682 \\
		GRU (augmented by 3) & 0.9601 & 0.9674 & 0.9691 & 0.9683 \\
		\hline
		Attn (original) & 0.9268 & 0.9123 & 0.9268 & 0.8966 \\
		Attn (augmented by 2) & 0.9529 & 0.9399 & 0.9529 & 0.9279 \\
		Attn (augmented by 3) & 0.9529 & 0.9399 & 0.9529 & 0.9279 \\
		\bottomrule\\
	\end{tabular}
	\caption{Performance of the recurrent models using both the original and the augmented datasets}
	\label{tab:recurrent_models_results}
\end{table}

\section{Evaluation and discussion of the segmentation task} % (fold)
\label{sec:evaluation_of_the_segmentation_task_recurrent}
Using the same heuristic described in Section~\ref{sec:sentence_clf_segmentation}, we evaluated the text segmentation task over the test set of annotated cases using the gold standard described in Section~\ref{sec:the_gold_standard}. Results are shown in Tables~\ref{tab:lstm_results}, \ref{tab:gru_results}, and \ref{tab:attn_results}, and Figures~\ref{fig:lstm_results} through \ref{fig:attn_results_augm} in Appendix~\ref{app:appendixA}.	

As the tables and figures show, the recurrent architectures are able to produce a higher incidence of documents whose segmentation differs by a single sentence from the gold standard, while, unlike the Sentence Classifier method (see Chapter~\ref{ch:segmentation_sentence_clf}), also producing a much more evenly spread distribution of offsets. For example, Table~\ref{tab:gru_results_0} shows that the highest number of documents segmented precisely at the point indicated by the gold standard (the figure in bold text, 468) corresponds to $\gamma=1.4$, which corresponds to 70\% of the dataset, though the percentage of documents offset by at most 1 sentence ($33+468+16$) is 78\%, which is considerably higher than the performance obtained with the Sentence Classifier method (see Section~\ref{sec:evaluation_of_the_segmentation_task_sentence}). Additionally, Figure~\ref{fig:gru_results} shows the trend of increasing values of $\gamma$ corresponding to an increased performance; the greater the value of $\gamma$, the greater the number of documents segmented exactly where the gold standard indicated.

The augmented dataset using PLSDA (see Section~\ref{sec:data_augmentation}), once again, shows no significant improvement in performance, compared to the original dataset. See Figures~\ref{fig:attn_results} and \ref{fig:attn_results_augm}. See Appendix~\ref{app:appendixA} for additional figures illustrating the performance of the other models.


It is interesting to note that, unlike the Sentence Classifier method, increasing the value of $\gamma$ leads to an increase in performance, as evidenced in Figures~\ref{fig:attn_results} and \ref{fig:attn_results_augm}, where the fraction of segmented documents whose offset is at most 1 increases as $\gamma$ increases. This suggests that, despite its better performance in the intrinsic evaluation (see Section~\ref{sec:evaluation_of_the_recurrent_architectures}), the recurrent architecture models continue to underestimate the splitting point of a given document. The high intrinsic evaluation also casts additional doubt on the original assumption that \textit{Facts} and \textit{non-Facts} are contiguous and that the former must always precede the latter: similarly to what we observed in Section~\ref{sec:evaluation_of_the_segmentation_task_sentence} high-precision and recall values do not account for the issue of the considerable fraction (17\% for $\gamma=1.4$ and even higher for lower values) of documents whose splitting index is underestimated by more than 4 sentences. This fraction is, nevertheless, a considerable improvement over the one obtained using the Sentence Classifier method (79\% for $\gamma=1.4$), suggesting, ultimately, that the recurrent nature of the models described in this chapter, which account for the semantic dependency of each sentence on its context, are a better approach at segmenting a document into \textit{Facts} and \textit{non-Facts}.

% , and Figures~\ref{fig:lstm_results} through \ref{fig:gru_results_augm}

Finally, the reduced performance produced by the use of the PLSDA technique with the LSTM/GRU model suggests that the overall performance of all three models would not significantly increase if there were more data available for training.

\begin{table}
	\centering
	\subfloat[Original dataset\label{tab:gru_results_0}]{\input{chapters/ch5/pictures/segm_lstm}}

	\subfloat[Dataset augmented by a factor of 2]{\input{chapters/ch5/pictures/segm_lstm_augm}}

	\subfloat[Dataset augmented by a factor of 3]{\input{chapters/ch5/pictures/segm_lstm_augm_2}}
	
	\caption{Sentence offset results for the different values of $\gamma$ using the LSTM network, and the original and augmented datasets}
	\label{tab:lstm_results}
\end{table}
\
\begin{table}
	\centering
	\subfloat[Original dataset]{\input{chapters/ch5/pictures/segm_gru}}

	\subfloat[Dataset augmented by a factor of 2]{\input{chapters/ch5/pictures/segm_gru_augm}}
	
	\subfloat[Dataset augmented by a factor of 3]{\input{chapters/ch5/pictures/segm_gru_augm_2}}
	
	\caption{Sentence offset results for the different values of $\gamma$ using the GRU network, and the original and augmented datasets}
	\label{tab:gru_results}
\end{table}

\begin{table}
	\centering
	\subfloat[Original dataset]{\input{chapters/ch5/pictures/segm_attn}}

	\subfloat[Dataset augmented by a factor of 2]{\input{chapters/ch5/pictures/segm_attn_augm}}
	
	\subfloat[Dataset augmented by a factor of 3]{\input{chapters/ch5/pictures/segm_attn_augm_2}}
	
	\caption{Sentence offset results for the different values of $\gamma$ using the seq2seq Attention mechanism, and the original and augmented datasets}
	\label{tab:attn_results}
\end{table}

\begin{figure}
	\centering
	\subfloat[0.5\linewidth][$\gamma=0.6$]{
	\scalebox{0.8}{\input{chapters/ch5/pictures/gru_gamma06}}
	}
	\subfloat[0.5\linewidth][$\gamma=0.8$]{
	\scalebox{0.8}{\input{chapters/ch5/pictures/gru_gamma08}}
	}

	\subfloat[0.5\linewidth][$\gamma=1$]{
	\scalebox{0.8}{\input{chapters/ch5/pictures/gru_gamma10}}
	}
	\subfloat[0.5\linewidth][$\gamma=1.2$]{
	\scalebox{0.8}{\input{chapters/ch5/pictures/gru_gamma12}}
	}

	\subfloat[0.5\linewidth][$\gamma=1.4$]{
	\scalebox{0.8}{\input{chapters/ch5/pictures/gru_gamma14}}
	}
	\caption{Sentence offset distribution for the different values of $\gamma$ using a GRU network on the original dataset}
	\label{fig:gru_results}
\end{figure}

\begin{figure}
	\centering
	\subfloat[0.5\linewidth][$\gamma=0.6$]{
	\scalebox{0.8}{\input{chapters/ch5/pictures/gru_gamma06_augm}}
	}
	\subfloat[0.5\linewidth][$\gamma=0.8$]{
	\scalebox{0.8}{\input{chapters/ch5/pictures/gru_gamma08_augm}}
	}

	\subfloat[0.5\linewidth][$\gamma=1$]{
	\scalebox{0.8}{\input{chapters/ch5/pictures/gru_gamma10_augm}}
	}
	\subfloat[0.5\linewidth][$\gamma=1.2$]{
	\scalebox{0.8}{\input{chapters/ch5/pictures/gru_gamma12_augm}}
	}

	\subfloat[0.5\linewidth][$\gamma=1.4$]{
	\scalebox{0.8}{\input{chapters/ch5/pictures/gru_gamma14_augm}}
	}
	\caption{Sentence offset distribution for the different values of $\gamma$ using a GRU network on the dataset augmented by a factor of 2}
	\label{fig:gru_results_augm}
\end{figure}

\begin{figure}
	\centering
	\subfloat[0.5\linewidth][$\gamma=0.6$]{
	\scalebox{0.8}{\input{chapters/ch5/pictures/gru_gamma06_augm_2}}
	}
	\subfloat[0.5\linewidth][$\gamma=0.8$]{
	\scalebox{0.8}{\input{chapters/ch5/pictures/gru_gamma08_augm_2}}
	}

	\subfloat[0.5\linewidth][$\gamma=1$]{
	\scalebox{0.8}{\input{chapters/ch5/pictures/gru_gamma10_augm_2}}
	}
	\subfloat[0.5\linewidth][$\gamma=1.2$]{
	\scalebox{0.8}{\input{chapters/ch5/pictures/gru_gamma12_augm_2}}
	}

	\subfloat[0.5\linewidth][$\gamma=1.4$]{
	\scalebox{0.8}{\input{chapters/ch5/pictures/gru_gamma14_augm_2}}
	}
	\caption{Sentence offset distribution for the different values of $\gamma$ using a GRU network on the dataset augmented by a factor of 3}
	\label{fig:gru_results_augm_2}
\end{figure}
% section evaluation_of_the_segmentation_task (end)
% section evaluation_of_the_recurrent_architectures (end)
% section lstm_and_gru (end)