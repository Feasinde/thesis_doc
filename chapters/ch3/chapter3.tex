%! TEX root = main.tex
\chapter{The dataset}
\label{ch:dataset}

In this chapter, we describe the dataset we used to perform the text segmentation task described in Chapter~\ref{ch:introduction}. We describe the way by which the original dataset provided by the \textit{Régie du logement} of the city of Montréal is mined for annotated training instances. After this, we describe the manner in which we build a suitable gold standard. Finally, we describe the process of Part of Speech-based Lexical Substitution Data Augmentation (PLSDA) \parencite{xiang2020lexical}, a technique of data augmentation that we used to increase the amount of training instances in our annotated corpus. 

\section{The original dataset} % (fold)
\label{sec:the_original_dataset}

% section the_original_dataset (end)

JusticeBot \parencite{westermann2019using} is an AI tool developed to simplify public access to legal information. It is an interactive system where the user is asked a set of questions about the issue in question and is given information related to previous similar cases. It was developed using a corpus of 667,305 decisions in French, provided by the \textit{Régie du Logement} of the city of Montréal. One of the numerous tasks related to the development and training of the chatbot is the extraction of case-related facts from a given document. Figure~\ref{fig:example_document} shows an example document. Statistics of the original dataset are given in Table~\ref{tab:original_corpus_stats} \parencite{salaun2020multilabel}.

\begin{table}
\centering
	\begin{tabular}{lr}
	\toprule\\
	Number of documents & 667,305 \\
	Av number of words & 363 $\pm$ 434 \\
	Number of distinct types & 337,491 \\

	\bottomrule\\
	\end{tabular}
	\caption{Statistics of the original dataset}
	\label{tab:original_corpus_stats}
\end{table}

As Figure~\ref{fig:example_document} shows, the original author of the ruling (the judge), naturally delimited Facts and Analysis sections through the use of appropriate headings. However, most of these documents do not clearly separate these two sections. Therefore, our first task consisted in creating an annotated corpus out of this original corpus.

\begin{figure}
	\centering
	\input{chapters/ch3/pictures/example_document}
	\caption[Example of a document from the original dataset.]{Example of a document from the original dataset. \textit{Facts} and \textit{non-Facts} sections are separated by headings.}
	\label{fig:example_document}
\end{figure}


\section{Extraction of annotated cases} % (fold)
\label{sec:extraction_of_annotated_cases}

The dataset used for fact extraction consists of a subset of the \textit{Régie du Logement}'s corpus and includes 5,605 unique annotated rulings; these were automatically selected from the original dataset because they include explicit separation between two distinct sections: Facts and Analysis.

In order to extract an annotated corpus from the original, unannotated corpus provided by the \textit{Régie du logement}, we used a heuristic based on regular expressions that matched common headings titles that denoted an explicit separation of case-related facts and judicial analysis. As stated in Section~\ref{sec:the_original_dataset}, the vast majority of documents (99.4\%) in the unannotated corpus contain text with no explicit demarcation between the facts that led to the judicial decision and the analysis and application of these; and those that did use such demarcation follow an irregular pattern, with some using headings and some simply using paragraph breaks.

We developed regular expressions to extract the annotated documents based on the most common headings that unambiguously denoted explicit demarcation between \textit{Facts} and \textit{non Facts}. We experimented with several different combinations of common phrases in order to take advantage of decisions using paragraph breaks as delimiters, but we consistently obtained ``noisy'' datasets, that is, datasets containing several hundreds of instances where unexpected formatting styles (eg arbitrary capitalisation, unexpected punctuation, unexpected itemisation, etc) would yield documents whose segmentation was not clear at all. Since precision was more important than recall, we preferred to have a less noisy annotated corpus (albeit smaller) than a larger one that may be more noisy. Hence, we chose to keep only two phrases that unequivocally yielded easily segmented documents; these were \textit{Les faites pertinents} (``The pertinent facts'') and \textit{Analyse} (``Analysis''). Using these as the basis of our regular expressions, we obtained 6,595 annotated documents (0.66\% of the original corpus); after removing duplicates, we were left with 5,605 documents. The small fraction of annotated documents is indicative of the problem described in Section~\ref{sec:problem_description}.

Statistics of the resulting annotated dataset are shown in Table~\ref{tab:dataset_stats}. As the table shows, the dataset we obtained includes 454,210 sentences annotated as either Facts or non-Facts, split approximately 55-45\% respectively. As Table~\ref{tab:dataset_stats} shows, Fact and non-Fact segments are similar both in terms of average number of words per sentence and average number of sentences per segment.


\begin{table}
\centering
	\begin{tabular}{lr}
	\toprule
	Number of documents & 5,605\\
	Total number of sentences & 454,210\\
	\hline
	Total number of sentences in \textit{Facts} segments & 239,077\\
	Av number of sentences in \textit{Facts} segments & 36.25\\
	Av number of words in \textit{Facts} segments & 832.08\\
	\hline
	Total number of sentences in \textit{non-Facts} segments & 215,133\\
	Av number of sentences in \textit{non-Facts} segments & 32.62\\
	Av number of words in \textit{non-Facts} segments & 803.74\\
	\bottomrule\\
	\end{tabular}
	\caption{Statistics of the annotated dataset used in our work.}
	\label{tab:dataset_stats}
\end{table}


\section{The gold standard} % (fold)
\label{sec:the_gold_standard}

% The segmentation task must be evaluated against a standard that meets two requirements:

In order to evaluate the proposed segmentation methods in this thesis, they must be evaluated against a test set that meets two requirements:

\begin{enumerate}
	\item The segment containing the \textit{Facts} is labelled and clearly demarcated from the rest of the sentences.
	\item The dataset only contains a sequence of sentences without headings to distinguish the \textit{Facts} from the \textit{non-Facts}. Otherwise, the segmentation would be trivial.
\end{enumerate}

Since the annotated corpus we extracted complied with the first condition but not with the second, we further processed it to create a test set that would meet both criteria; this test set will also be called the \textit{gold standard} for the rest of this thesis.

To create the gold standard, each annotated document was stripped from its headings, and the text contained in each section was concatenated to form a continuous sequence of sentences. As Figure~\ref{fig:doc2string_example} shows, we remove line breaks, dashes, and semicolons, replacing them with full stops. We then split the text into sentences using the spaCy NLP library \parencite{honnibal2017spacy}. Finally, we associate each resulting paragraph to a binary sequence in which each digit corresponds to a sentence which is either found in the \textit{Facts} (1) or the \textit{non-Facts} (0) creating a binary tensor of length equal to the number of sentences in the document. The resulting binary sequences serve as targets for the different approaches we followed to perform the text segmentation.

\subsection{Limitations of the gold standard} % (fold)
\label{sub:limitations_of_the_gold_standard}
The manner in which the gold standard is created is non-ideal for one important reason: The set of documents retrieved from the original corpus deemed to be annotated, as detailed in Section~\ref{sec:extraction_of_annotated_cases}, is limited by our ability to craft relevant regular expressions that select relevant samples. There might exist documents which we could readily segment into facts and non-facts that were not retrieved by our method, which was reliant on explicit headings appearing in the text. Nevertheless, we considered that such method was the best option to ensure the retrieval of documents which could be automatically segmented with little ambiguity.
% subsection limitations_of_the_gold_standard (end)

% Figure~\ref{fig:doc2string_example} illustrates the process of creating a gold standard binary sequence.

\begin{figure}
	\centering
	\input{chapters/ch3/pictures/doc2string}
	\caption[Creation of the gold standard.]{Creation of the gold standard. To create an instance, we remove line breaks, dashes, and semicolons and replace them with headings; we then remove the headings and concatenate the segments; we then split the sentences, and, finally represent the text as a binary sequence, where the Facts sentences are represented as a subsequence of 1's and the non-Facts sentences as a subsequence of 0's.}
	\label{fig:doc2string_example}
\end{figure}

\section{Data augmentation} % (fold)
\label{sec:data_augmentation}

\begin{figure}
	\input{chapters/ch3/pictures/plsda}
	\caption[Example of the listing of substitute candidates for words in a sentence.]{Example of the listing of substitute candidates for words in a sentence. Each word in the sentence is tagged with its POS tag and associated with a list of substitute candidates mined from WordNet. A candidate is probabilistically chosen to create a new sentence that is syntactically identical and semantically similar to the original sentence.}
	\label{fig:PLSDA}
\end{figure}

In order to account for the relatively low number of training instances in our annotated corpus, we turned to a data augmentation technique called POS-based Lexical Substitution Data Augmentation (PLSDA). Introduced by \textcite{xiang2020lexical}, the principle behind PLSDA is simple: Given an input sentence, a list of substitute candidates is created for each word in the sentence, based on the corresponding Part-of-Speech (POS) tag of the word, and using tools such as WordNet \parencite{miller1995wordnet} to obtain a list of syntactically equivalent synonyms. Using a probabilistic approach to select the substitutions, a new training instance is generated each time a substitution occurs in the original training instance. 

For example, consider the phrase \textit{Deux hommes entre tous les hommes ont le droit de répondre} (``Two men, amongst all men, have the right to answer.''). Each of the words in the sentence is tagged with its corresponding POS tag, as indicated in Figure~\ref{fig:PLSDA}, and using this information, a set of synonyms is extracted from WordNet for each of the annotated tokens. Because of syntactic consistency, if we were to replace any token with another, identically tagged token (eg replace \textit{hommes}, ``men'', with \textit{mâles adultes}, ``adult males''), the resulting sentence would be syntactically identical to the original; additionally, because of semantic consistency, if we were to replace a token with a synonym with an identical POS tag, the resulting sentence will have similar semantics to the original sentence, thus rendering it a potential training instance for our task.

Because of the relative scarcity of resources in the French section of multilingual WordNet, we modified the procedure used by \textcite{xiang2020lexical} and selected only nouns and adjectives as candidates for substitutions. Nevertheless, we were able to approximately double and triple the number of documents and the number of sentences in our dataset. The selection of the substitution candidates was done as follows: for each sentence in dataset, the token with the most synonyms was the one selected for substitution, and the synonym was randomly selected from the associated synset following a uniform distribution. The substitution was carried out only one token at a time per sentence.

% Because of time constraints and the relative scarcity of resources in the French part of multilingual WordNet, we modified the procedure used by \textcite{xiang2020lexical} and selected only nouns and adjectives as candidates for substitutions. Nevertheless, almost each document in our corpus produced one additional training instance, and we were able to approximately double the size of our dataset (12,200 total instances). The selection of the substitution candidates was done as follows: for each sentence in dataset, the token with the most synonyms was the one selected for substitution, and the synonym was randomly selected from the associated synset following a uniform distribution. Table~\ref{tab:dataset_stats_augm} shows statistics of the augmented dataset.

\begin{table}
	\centering
	\begin{tabular}{lr|r|r}
	\textbf{Final datasets} & 1 & 2 & 3 \\
	\toprule
	Number of documents & 5,605 & 11,210 & 16,815\\
	Total number of sentences & 454,210 & 838,347 & 1,257,519\\
	\hline
	Total number of sentences in Facts segments & 239,077 & 442,481 & 717,231\\
	Av number of sentences in Facts segments & 36.25 & 36.25 & 36.25\\
	Av number of words in Facts segments & 832.08 & 832.08 & 832.08\\
	\hline
	Total number of sentences in non-Facts segments & 215,133 & 395,866 & 593,799\\
	Av number of sentences in non-Facts segments & 32.62 & 32.62 & 32.62\\
	Av number of words in non-Facts segments & 803.74 & 803.74 & 803.74\\
	\bottomrule\\
	\end{tabular}
	\caption[Statistics of the dataset augmented using PLSDA.]{Statistics of the dataset augmented using PLSDA. ``Augment'' refers to the factor by which the dataset was increased.}
	\label{tab:dataset_stats_augm}
\end{table}

\section{Chapter summary} % (fold)
\label{sec:summary_3}
In this chapter we described the original corpus from the \textit{Régie du Logement} that we used in this thesis and how we mined from it the annotated training instances in order to build an annotated corpus with which we could train and evaluate our models. We also described the process by which we build the appropriate gold standard with which we evaluate the segmentation task. Finally, we introduced and described the PLSDA technique \parencite{xiang2020lexical}, which we applied to our annotated corpus in order to increase the number of training instances.

In the next chapter, we will describe the first approach we developed to segment legal texts into \textit{Facts} and \textit{non-Facts}.
% section summary_3 (end)