\chapter{Related work}
\label{ch:related_work}

This chapter will expose the work that has been done at the intersection of Law and Natural Language Processing (NLP) and present a brief introduction to the neural methods used in this thesis to implement the methodologies described in Chapters~\ref{ch:segmentation_sentence_clf} and \ref{ch:segmentation_recurrent}. Section \ref{sec:natural_language_processing_in_legal_environments} will describe the application of traditional NLP methods for a variety of tasks involving the legal domain. Section~\ref{sec:text_segmentation_and_text_classification} will describe work done in Sentence Classification and Text Segmentation. Section~\ref{sec:neural_methods_for_nlp} will present an overview of recent developments in neural methods for NLP: Section~\ref{sub:ann_rnn_cnn} will describe the neural models used in our proposed first approach, based on the context-independent binary classification of individual sentences, while Sections~\ref{sub:encoder_decoder} and \ref{sub:transformer_bert} will describe the models used for our second proposed approach, based on a recurrent architecture that allows for the content-dependent and context-dependent classification of sentences.

\section{Natural Language Processing in legal environments} % (fold)
\label{sec:natural_language_processing_in_legal_environments}

% section natural_language_processing_in_legal_environments (end)

The use of Natural Language Processing (NLP) to mine judicial corpora is not new; however, very little work has used neural methods, as most of the cited literature uses rules or hand-crafted features to perform their stated tasks. \textcite{de2006automated} developed a parser that automatically extracts reference structures (which is loosely equivalent to the notion of named entities) in and between legal sources. A few years later, \textcite{de2009next} developed a model based on syntactic parsing that automatically classifies norms in legislation by recognising typical sentence structures. \textcite{dell2012splet} proposed a shared task on dependency parsing of legal texts and domain adaptation where participants compared different parsing strategies utilising the DeSR parser \parencite{attardi2006experiments} and a weighted directed graph-based model \parencite{sagae2010dependency}. \textcite{grabmair2015introducing} demonstrated the feasibility of extracting argument-related semantic information and used it to improve document retrieval systems. \textcite{walker2017semantic} introduced an annotated dataset based on propositional connectives and sentence roles in veterans' claims, wherein each document is annotated using a typology of propositional connectives and the frequency of the sentence types that led to adjudicatory decisions. \textcite{savelka2017using} used Conditional Random Fields (CRF) to extract sentential and non-sentential content from decisions of United States courts, and also extract functional and issue-specific segments \parencite{savelka2018segmenting}, achieving near-human performance. Finally, \textcite{dragoni2018nlprule} performed rule-extraction from legal documents using a combination of a syntax-based system using linguistic features provided by WordNet \parencite{miller1995wordnet}, and a logic-based system that extracts dependencies between the chunks of their dataset. Their work is closely related to our task; however, whereas \textcite{dragoni2018nlprule} base their model on syntactic and logical rulesets, we base our model on Recurrent and Convolutional Neural models (see Sections~\ref{sub:recurrent_neural_networks_lstm_and_gru} and \ref{ssub:convolutional_neural_networks}), introduce our own segmentation heuristic (see Section~\ref{sec:sentence_clf_segmentation}), and use independent  and contextual word embeddings \parencite{mikolov2013efficient, 2019arXiv191103894M}.

Of note is the work of \textcite{campbell2018procezeus}, who developed \textit{ProceZeus}, a chatbot intended to facilitate access to judicial proceedings involving rental housing issues in Québec. ProceZeus is able to build an input case by asking a series of questions to the user and afterwards presenting the most similar cases to that of the user using $k$-Nearest Neighbours \parencite{fix1952discriminatory}, predicting the potential monetary compensation using regression, and using Support Vector Machines \parencite{cortes1995support} to predict the outcome of the case.
% We will now discuss in more detail the tasks of text segmentation and text classification.

\section{Text segmentation and text classification} % (fold)
\label{sec:text_segmentation_and_text_classification}

% section text_segmentation_and_text_classification (end)

Most sentence classification tasks in the literature are concerned with sentiment analysis. Our task, in contrast, is annotated following an arguably less intuitive scheme: whether a given sentence seems to state a case fact or if it represents a part of the judicial analysis (discussed in more detail in Chapter~\ref{ch:dataset}). Sentiment analysis itself is faced with the constant issue of inter-annotator disagreement (cf \parencite{westermann2019using}, \parencite{mozetivc2016multilingual} and~\parencite{bermingham2009study}), and while sentiment analysis and opinion mining have been very popular for many years now (see \parencite{pang2008opinion} for a detailed survey of the literature), classification tasks involving more abstract, or less polarised, categories are not nearly as common.

Text segmentation is a task that consists of dividing a document into meaningful syntactic or semantic units at the word or sentence levels. Topic modelling, in particular, has been a fundamental task of the field for some time now \parencite{ponte1997text}, and it is by far the most common application of text segmentation. As part of their Topic Detection and Tracking study, \textcite{allan2003topic} developed a model that segments a stream of text from speech transcriptions using Hidden Markov Model techniques. Similarly, \textcite{beeferman1999statistical} introduced statistical methods in the form of featured-based models that predict text boundaries using the metric of \textit{topicality}, a measure of the ratio of predictions between a long-range language model and a trigram model. In contrast to the domain-specific nature and use of lexical cohesion of these works, \textcite{utiyama2001statistical} developed a domain-independent segmentation model intended to be used on text summarisation based solely on statistical language models, which was shown to match and even outperform previous work. Recent work based on stochastic methods mostly use models based on Latent Dirichlet Allocation, a statistical generative model where each item in a collection is modelled as an infinite mixture over an underlying set of topics \parencite{blei2003latent,misra2009text}. In contrast to this previous work, our segmentation method is based on accounting for the varying density of factual content across the many possible segmentations that can be produced from a single text.

In order to better understand our proposed approaches, we will now briefly introduce the foundations and the principles of the neural methods we used to carry out the task of text segmentation.

\section{Neural methods for NLP} % (fold)
\label{sec:neural_methods_for_nlp}

% section neural_methods_for_nlp (end)

Outside the frame of legal texts, semantic sentence classification has recently achieved new benchmarks thanks to the application of neural methods. The use of Recurrent Neural Networks (RNN), and their derivative recurrent architectures (in particular encoder-decoder models \parencite{cho2014learning}), has steadily produced results that outperform traditional models in many NLP tasks such as Machine Translation, Question Answering and Text Summarisation (eg the Attention mechanism \cite{bahdanau2014neural} and the Sequence-to-Sequence model \parencite{sutskever2014sequence}, two of the most important architectural developments in RNNs). Sentence classification using Deep Learning (DL) was first proposed by \textcite{kim2014convolutional}, who used Convolutional Neural Networks (CNN) and showed that their models outperformed classical models on many standard datasets. Their work was quickly followed by the application of RNN architectures for similar tasks, including those of \textcite{lai2015recurrent}, \textcite{zhang2015character}, and \textcite{zhou2016text}. A major breakthrough was achieved with the Transformer \parencite{vaswani2017attention} (see Section~\ref{sub:transformer_bert}), whose non-recurrent, Multi-Head Attention architecture allowed for richer language model representations that capture many more linguistic features than the original attention mechanism. Subsequently, Google's BERT \parencite{devlin2018bert} has given way to a whole new family of language models able to produce state-of-the-art contextual embeddings for both individual tokens in a sentence and for the sentence itself. The following paragraphs will explain these architectures in detail.


\subsection{Basic neural architectures: Artificial neural networks, recurrent networks and convolutional networks} % (fold)
\label{sub:ann_rnn_cnn}

An Artificial Neural Network (ANN) is a mathematical model inspired by the biology of animal brains, though its concept and design have drifted considerably from any kind of biological resemblance. In this section we briefly describe the three basic architectures of ANNs we used to perform sentence classification as part of the task of Fact-extraction:  Feedforward Neural Networks (FNN), Recurrent Neural Networks (RNN) and Convolutional Neural Networks (CNN).

\subsubsection{Feedforward Neural Networks (FNN)} % (fold)
\label{ssub:feedforward_neural_networks}
FNNs are neural networks where no cyclic structures occur when moving the information forward. It is the simplest model of ANNs.

Let $\mathbf{x} \in \mathbb{R}^n$ be a vector, $\mathbf{W} \in \mathbb{R}^{m \times n}$ a matrix of real-valued parameters, and $\mathbf{b} \in \mathbb{R}^m$ a real-valued vector, the bias. A one-layer FNN $f$ computes the non-linear transformation resulting from the matrix product of $x$ and $W$, the addition of a bias term, and passing the result through a non-linear activation function $g$:

\begin{align*}
	f(\mathbf{x}) &= g(\mathbf{Wx} + \mathbf{b}) \\
\end{align*}

A multi-layer FNN computes more than one of such transformations:

\begin{align*}
	\mathbf{h} &= g(\mathbf{W_1x} + \mathbf{b_1}) \\
	f(\mathbf{x}) &= g(\mathbf{W_2h + b_2})
\end{align*}

Where $\mathbf{h}$ is a real-valued vector of arbitrary dimension representing the \textbf{hidden layer} of $f$. Deep networks consist of models with multiple hidden layers. Figure~\ref{fig:FNN} provides a visual representation of a FNN.

\begin{figure}
	\centering
	\input{chapters/ch2/pictures/fnn}
	\caption[Visual representation of a two-layer feedforward neural network]{Visual representation of a two-layer feedforward neural network, where $x_1, x_2, \hdots, x_n \in \mathbf{x}$, $h_1, h_2, \hdots, h_m \in \mathbf{h}$, and $y_1, y_2, \hdots, y_l \in f(\mathbf{x})$ }
	\label{fig:FNN}
\end{figure}
% subsubsection feedforward_neural_networks (end)

\subsubsection{Recurrent Neural Networks (RNN)} % (fold)
\label{sub:recurrent_neural_networks_lstm_and_gru}
RNNs are neural models designed to take input of a sequential nature. In addition to a similar feed-forward component, RNNs include a \textbf{hidden state} vector that makes the output of a token at a given time a function of both its input and the input at the previous step.

The basic form of a RNN is as follows: Let $x = (\mathbf{x}_1, \mathbf{x}_2, \hdots, \mathbf{x}_t)$ be a sequence, and let each $\mathbf{x_i}$ be an $n$-dimensional vector; let $\mathbf{W_h} \in \mathbb{R}^{m \times n}$, $\mathbf{W_y} \in \mathbb{R}^{l \times p}$ and $\mathbf{U} \in \mathbb{R}^{m \times p}$ be matrices of real-valued parameters; let $\mathbf{b_h} \in \mathbb{R}^m$ and $\mathbf{b_y} \in \mathbb{R}^l$ be bias vectors, and let $\mathbf{h}_t \in \mathbb{R}^p$ be the hidden state at time $t$. Then, for a RNN $f$ and an activation function $g$,

\begin{align*}
	\mathbf{h}_t &= g(\mathbf{W_hx} + \mathbf{Uh}_{t-1} + \mathbf{b_h}) \\
	\mathbf{f(x)_t} &= g(\mathbf{W_yh}_t + \mathbf{b_y})
\end{align*}

where $\mathbf{f(x)}_t$ is the $l$-dimensional output at time $t$. Figure~\ref{fig:recurrent_network} shows a visual representation of an RNN.

\begin{figure}
	\centering
	\input{chapters/ch2/pictures/recurrent}
	\caption[Visual representation of a recurrent neural network.]{Visual representation of a recurrent neural network. The recurrent unit, in green, is responsible for transmitting the information across time to the next recurrent unit in the sequence.}
	\label{fig:recurrent_network}
\end{figure}

The basic RNN is seldom used nowadays, having been largely superseded by Long Short-Term Memory (LSTM) networks \parencite{hochreiter1997long} and Gated Recurrent Unit networks (GRU) \parencite{cho2014learning}. Both models make use of ``gates'', that is, linear and non-linear transforms aimed at preserving and discarding information produced in previous states.

\textbf{LSTM networks} were first introduced to address the problem of vanishing gradients, wherein the value of the gradient would become vanishingly small as it was computed during backpropagation. An LSTM unit is comprised of a \textbf{cell}, an \textbf{input gate}, an \textbf{output gate}, and, more recently, a \textbf{forget gate}. The name ``long short-term memory'' comes from the fact that the unit is able to arbitrarily maintain certain values as the information travels through time, unlike the basic RNN where no information can be retained intact once the time step is over. In effect, LSTMs are able to deal with long-distance dependencies such as those that occur in natural language. 

Here we present the compact version of the equations and gates involved in a single unit, and Figure~\ref{fig:lstm} presents a visualisation of the interaction of each of these vectors. \footnote{Additionally, we recommend \textcite{olah2020lstm}'s \href{http://colah.github.io/posts/2015-08-Understanding-LSTMs/}{excellent guide to understanding LSTMs.}}

\begin{align*}
	\mathbf{f}_t &= g(\mathbf{W_f} \mathbf{x}_t + \mathbf{U_f} \mathbf{h}_{t-1} + \mathbf{b_f}) \\
	\mathbf{i}_t &= g(\mathbf{W_{i}} \mathbf{x}_t + \mathbf{U_{i} h}_{t-1} + \mathbf{b_i}) \\
	\mathbf{o}_t &= g(\mathbf{W_{o} x}_t + \mathbf{U_{o} h}_{t-1} + \mathbf{b_o}) \\
	\mathbf{\tilde{c}}_t &= g_h(\mathbf{W_{c} x}_t + \mathbf{U_{c} h}_{t-1} + \mathbf{b_c}) \\
	\mathbf{c}_t &= \mathbf{f}_t \circ \mathbf{c}_{t-1} + \mathbf{i}_t \circ \mathbf{\tilde{c}}_t \\
	\mathbf{h}_t &= \mathbf{o}_t \circ g_h(\mathbf{c}_t)
\end{align*}

\begin{figure}
	\centering
	\input{chapters/ch2/pictures/lstm}
	\caption{Visual representation of an LSTM unit with a forget gate, the most common variant}
	\label{fig:lstm}
\end{figure}

The problem of vanishing gradients and long-distance dependencies has also been addressed with GRU. First introduced in the context of Machine Translation, \textbf{GRU networks} achieve performances similar to LSTMs \parencite{chung2014empirical,greff2016lstm} while also being comparatively simpler; the most important modification is the merging of the ``forget'' and the ``input'' gates into a single \textbf{update} gate. Again, we present the compact equations below and Figure~\ref{fig:gru} presents a visualisation of the GRU unit.

\begin{align*}
	\mathbf{z}_t &= g(\mathbf{W_z} \cdot [\mathbf{h}_{t-1}, \mathbf{x}_t]) \\
	\mathbf{r}_t &= g(\mathbf{W_r} \cdot [\mathbf{h}_{t-1}, \mathbf{x}_t]) \\
	\mathbf{\tilde{h}}_t &= \tanh(\mathbf{W} \cdot [\mathbf{r}_t \ast \mathbf{h}_{t-1}, \mathbf{x}_t]) \\
	\mathbf{h}_t &= (1- \mathbf{z}_t) \ast \mathbf{h}_{t-1} + \mathbf{z}_t \ast \mathbf{\tilde{h}}_t
\end{align*}

\begin{figure}
	\centering
	\input{chapters/ch2/pictures/gru}
	\caption{Visual representation of a fully gated GRU unit, the most common variant}
	\label{fig:gru}
\end{figure}

Regardless of the choice of recurrent unit, a recurrent architecture will follow the same design illustrated in Figure~\ref{fig:recurrent_network}

Recurrent Neural Networks are useful when dealing with data presented as a time-sensitive sequence. However, they are constrained by the fact that the length of its output must match the length of its input. NLP tasks, such as Machine Translation (ML) require time-sensitive models that are able to handle different lengths between input and output; for example, the Sindarin phrase \textit{Ónen i-Estel Edain} (4 tokens), which is translated as \textit{I gave hope to Men} (5 tokens in English) would be impossible to model using a traditional RNN because of the mismatch between the length of the input and output sequences. In order to work around this constraint, we will use the Encoder-Decoder model.

\subsubsection{Encoder-Decoder} % (fold)
\label{sub:encoder_decoder}
To deal with the constraint of having the same dimensionality in both input and output vectors in RNNs, \textcite{sutskever2014sequence} introduced the Sequence-to-Sequence (seq2seq) model, a recurrent model able to model input and output sequences of different lengths. The seq2seq model uses a novel architecture ensemble called the Encoder-Decoder. The \textbf{Encoder} is a model that produces a \textit{context vector} that contains a summary of the entire input sequence; the \textbf{Decoder} is a second model, usually a recurrent network, that decodes the context vector and produces a corresponding output sequence, which need not have the same length as the input. Figure~\ref{fig:encoder_decoder} shows a visual representation of the Encoder-Decoder.

\def\amp{\pgfmatrixnextcell}
\begin{figure}[ht!]
	\input{chapters/ch2/pictures/encoder_decoder}
	\centering
	\caption[Visual representation of an Encoder-Decoder.]{Visual representation of an Encoder-Decoder. The input sequence, of different length than the output, is encoded into a context vector ($\mathbf{h}_4 = \mathbf{s}_0$), which is then decoded as the output sequence.}
	\label{fig:encoder_decoder}
\end{figure}

Several methods of encoding the context vector have been proposed. Figure~\ref{fig:encoder_decoder} illustrates the original method proposed by \textcite{sutskever2014sequence} and \textcite{cho2014learning} to compute the context, using the final hidden state of the input sequence as the initial hidden state of the output sequence (ie $\mathbf{h}_4 = \mathbf{s}_0$).

\subsubsection{Attention} % (fold)
\label{ssub:attention}
If a long input sequence is processed by an Encoder-Decoder, the intermediate context vector is burdened with having to encode much information into, usually, a fixed-dimension representation. The farther an input token is from the final hidden state that corresponds to the context vector, the less information about this token will be included in the context; moreover, the context vector is usually not enough to capture long-distance dependencies between the tokens in the input. For example, languages with richer inflection than English oftentimes have to keep track of gender and number markers across the nouns of a sentence.

In order to deal with the overburdening of information into a single, fixed-length context vector, \textcite{bahdanau2014neural} introduced the Attention mechanism. At any given time step, the decoder uses three elements to produce an output: the previous hidden state, the output from the previous time step, and a weighted representation of the tokens in the input sequence. The weighted representation of the input sequence, called the \textit{annotations} in the original paper, acts as a measure of how important each of the input tokens is for the decoding process at the current time step; in other words, the decoder is told which tokens to pay attention to when producing the current output. Figure~\ref{fig:attention} shows a visual representation of the mechanism.

\begin{figure}[ht!]
	\centering
	\input{chapters/ch2/pictures/attn}
	\caption[Visual representation of an Encoder-Decoder using an Attention mechanism.]{Visual representation of an Encoder-Decoder using an Attention mechanism. At $t=3$, the word \textit{kept} is produced as the combination of the previous hidden state, the previous output, and the context vector corresponding to the weighted ($\alpha_i$) annotations of the input sequence.}
	\label{fig:attention}
\end{figure}

% subsection

\subsubsection{Convolutional Neural Networks} % (fold)
\label{ssub:convolutional_neural_networks}

\begin{figure}
	\input{chapters/ch2/pictures/cnn}
	\caption[Visual representation of a typical CNN architecture.]{Visual representation of a typical CNN architecture. In this example, the input image is swept by kernels (red, yellow, and blue) to produce the corresponding feature maps. These are subsampled to produce a second set of feature maps, which is passed through a feedforward network in order to produce a binary output.}
	\label{fig:CNN}
\end{figure}

CNNs are neural models designed to take in data where the context surrounding the input determines the value of the model's output. Originally, CNNs were introduced as means of dealing with visual media. Given a one or two-dimensional input over a number of channels, a number of tensors, each of the same dimensionality, called \textbf{kernels}, are swept over the surface of the image, performing the convolution operation over one set of pixels at a time. Each convolution yields a numerical value, and the set of convolutions produced by each kernel yields a \textbf{feature map}, an intermediate representation of the image; this can be further subsampled using different methods to produce another representation of the original input. An image can thus be processed by a deep convolutional network, where each layer produces a set of representations by means of convolution operations or subsampling. Figure~\ref{fig:CNN} shows a visual representation of the process.

CNNs can also be used to process text at the token or the character level, by sweeping a kernel over an input sequence and yielding feature map representations of the the text. The resulting representations contain limited, short-range contextual information of each of the tokens or characters in the sequence. Figure~\ref{fig:model_architecture} shows a visual representation of the process, where the input is a sequence of tokens and the convolution is performed over a fixed-width window of contiguous tokens.

\begin{figure}
	\centering
	\input{chapters/ch4/pictures/conv}
	\caption[Visual representation of text processing using a CNN.]{Visual representation of text processing using a CNN. The feature maps produced by the convolutional layers are short-range contextual representations of the input sequences.}
\end{figure}

\subsection{Word embeddings} % (fold)
\label{sub:language_embeddings}
Tokens cannot be fed directly into a neural model. They require a numerical representation, the most elementary of which is the \textbf{one-hot vector} representation: each word in the vocabulary is represented by a sparse binary vector whose length is the size of the vocabulary, and whose only non-zero element corresponds to the positional index of the word in the vocabulary. This method is simple and requires little computing power, but it does not capture any of the linguistic features of a given word, such as part-of-speech tag, case, tense, inflection, gender, etc. Moreover, the distribution of such vectors in their corresponding vector space offers no insight as to how closely “related” a set of words might be. For example, if the positional indices of words in the vocabulary followed an alphabetical sort, entries such as \textit{hopscotch, Horacio, indulge, intellectual, Jazz}, and \textit{Julio}, which bear no inherent linguistic relation amongst themselves, would all be relatively close to each other.

A better model of lexical vector representation is the \textbf{word embeddings} model, introduced by \textcite{mikolov2013efficient}. This model produces dense vectors of low dimensionality that, depending on the hyperparamenters of its training, preserve different linguistic features, such as number, gender, and semantics (eg synonyms are usually close to each other). Additionally, arithmetic operations between vectors in the set of embeddings are able to yield relations amongst the words in the vocabulary that a simple one-hot representation would be incapable of capturing. The time-honoured example of such operations is as follows: Let vectors $\mathbf{w}_1, \mathbf{w}_2, \mathbf{w}_3 \in \mathbb{R}^{n}$ correspond to the words \textit{king}, \textit{man}, and \textit{woman} respectively; then the simple arithmetic expression $\mathbf{w}_1 - \mathbf{w}_2 + \mathbf{w}_3 = \mathbf{x}$ yields a vector in the same vector space; using the metric of \textit{cosine distance} to find the nearest vector in the set, we find such vector, $\mathbf{w}_4$, which corresponds to the word \textit{queen}. In effect,

\begin{center}
\begin{tabular}{m{.8\linewidth}}
\begin{example}
\label{ex:king_queen}
	\[
		\text{vector(``king'')} - \text{vector(``man'')} + \text{vector(``woman'')} = \text{vector(``queen'')}
	\]
\end{example}
\end{tabular}
\end{center}

There are a number of drawbacks in the use of word embeddings. The greatest problem is the relatively large amount of data needed to train them, even though the process itself is \textit{unsupervised}, which means the data on which the vectors are trained need not be annotated with any kind of linguistic information. Another issue is implicit, real-world biases that might exist in the dataset. For example, following the hypothesis of \textit{distributional semantics} \parencite{harris1954distributional}, similar words in a corpus are found surrounded by similar contexts. While Example~\ref{ex:king_queen} is famous, a similarly infamous example is shown in Example~\ref{ex:doctor_nurse}:

\begin{center}
\begin{tabular}{m{.8\linewidth}}
\begin{example}
\label{ex:doctor_nurse}
	\[
		\text{vector(``doctor'')} - \text{vector(``man'')} + \text{vector(``woman'')} = \text{vector(``nurse'')}
	\]
\end{example}
\end{tabular}
\end{center}

Yet another drawback of word embeddings is their difficulty to deal with polysemy: words with identical spellings but different senses can be hard to disambiguate, especially if they share many linguistic features between themselves. For example, the word \textit{plant} is a genderless, singular, and caseless noun which might refer to either the taxonomy of living organisms, a particular member of such taxonomy, or to a human-made structure that generates power. Having a single word embedding for both senses does not distinguish each use.

Despite these hurdles, word embeddings pushed the state-of-the-art in all applications they were used in, and have only recently begun to be replaced by \textbf{contextual word embeddings} such as ELMO \parencite{peters2018deep} and BERT \parencite{devlin2018bert} (see Section~\ref{sub:transformer_bert}). Contextual word embeddings, as their name suggest, replace the unique vector-valued representation of a given word in the vocabulary with a dynamically generated word representation based on the input sentence in which the word is found.
% subsection language_embeddings (end)

% \subsection{Encoder-Decoder and Attention Mechanisms} % (fold)
% \label{sub:encoder_decoder_attention}
% An important development in RNNs was introduced in the form of the Encoder-Decoder architectures coupled with the seminal Attention Mechanism, introduced by \textcite{bahdanau2014neural}.


% subsubsection attention (end)


% subsection the_attention_mechanism (end)

\subsection{Contextual word embeddings: the Transformer, BERT, and BERT-like models} % (fold)
\label{sub:transformer_bert}
As an alternative to the original word embeddings model (see Section~\ref{sub:language_embeddings}), in which each word in the vocabulary is assigned a unique, context-independent dense vector, recent approaches to NLP have begun using contextual word and sentence embeddings produced by pre-trained language models such as BERT \parencite{devlin2018bert}. These have achieved state-of-art in many downstream tasks where the pre-trained model is attached to the relevant architecture and fine-tuned for its specific purposes. Before delving into BERT, however, we must discuss the architecture on which it is based, namely, the Transformer.

\subsubsection{The Transformer} % (fold)
\label{ssub:the_transformer}


The Transformer \parencite{vaswani2017attention} is a network architecture proposed to address the recurrent nature of sequence transduction models, ie sequential models that convert an input sequence into an output sequence, such as those used in Machine Translation and Question Answering. The recurrent architecture of these models precludes the use of parallelisation and, as such, oftentimes require higher costs in memory and processing power as the input sequences become longer.

The Transformer proposes a novel way of dispensing of sequential transduction models by means of solely applying the Attention mechanism (see Section~\ref{ssub:attention}) to capture global dependencies between the input and the output. Let $\mathbf{q, k} \in \mathbb{R}^{d_k}$ and $\mathbf{v} \in \mathbb{R}^{d_v}$ be vectors, called \textit{query, key} and \textit{value}, respectively. The Attention mechanism can be thought of as taking the dot-product of the query and the key, passing it through a softmax layer, and then taking the dot-product of the result and the value vector; in this setup, the query and key are used to calculate the attention weights, which are then applied to the annotations, represented by the value vector. The implementation used in the Transformer generalises this idea into using sets of queries, keys, and values and grouping them into corresponding matrices, while also adding a scaling factor in order to account for the growing magnitude of the dot-product for greater values of $d_k$.

\[
	\text{Attention}(\mathbf{Q,K,V}) = \text{softmax}\left( \frac{\mathbf{QK^T}}{\sqrt{d_k}} \right)\mathbf{V}
\]

This mechanism is called \textit{Scaled Dot-Product Attention} in the original paper, and is the basis for the central component of the Transformer, the \textit{Multi-head Attention} mechanism: each matrix is projected into different, learned linear projections of $d_k, d_k$ and $d_v$ dimensions respectively. These projections are passed through the Attention function, concatenated and finally projected one more time in order to compute the Multi-head Attention.

\begin{align*}
	\text{head}_i &= \text{Attention}(\mathbf{QW}^{\mathbf{(Q)}}_i, \mathbf{KW}^{\mathbf{(K)}}_i, \mathbf{VW}^{\mathbf{(V)}}_i)\\
	\text{MultiHead}(\mathbf{Q,K,V}) &= \text{Concat}(\text{head}_i, \hdots, \text{head}_h)\mathbf{W}
\end{align*}

The Transformer itself is presented as an Encoder-Decoder model, albeit one that does not use recurrent networks at all. Instead, the Encoder part is a stack of $N$ multi-head attention layers, each coupled with a feedforward network with a residual connection, that yield an output that is passed into the Decoder part, which is another stack of $N$ Multi-head attention layers, each also coupled with a feedforward network. Figure~\ref{fig:transformer} shows a visual representation of the Transformer Encoder and Decoder. \footnote{For a more detailed explanation on the inner workings of the Transformer, we recommend the excellent guide \textit{The Annotated Transformer} \parencite{rush2020transformer}.}

\begin{figure}
	\centering
	\input{chapters/ch2/pictures/transformer}
	\caption[Visual representation of the Transformer.]{Visual representation of the Transformer. Each feedforward network in the stacks is fed with both the output of the Multi-Head Attention sublayer and a residual connection from the previous layer in the stack. The Encoder part feeds an output into the Decoder, which also contains a Masked Multi-Head Attention layer in order to prevent current positions to attend to subsequent positions.}
	\label{fig:transformer}
\end{figure}

\subsection{BERT} % (fold)
\label{sub:bert}
BERT (Bidirectional Encoder Representations from Transformers) \parencite{devlin2018bert} is a pre-trained language representation model capable of producing richly represented contextual word and sentence embeddings. Its architecture consists of a stack of $L$ bidirectional Transformer encoders trained over an unannotated corpus using two tasks: Masked Language Model (Masked LM) and Next Sentence Prediction (NSP). Masked LM consists of having the model predict randomly masked tokens in its inputs, akin to the cloze procedure \parencite{taylor1953cloze}. NSP consists of a binarised test wherein two sentences, $A$ and $B$ are taken from the corpus and the model determines whether $A$ is followed by $B$ or not.

BERT's greatest advantage is its fine-tuning capabilities. It can be attached to any downstream task and fine-tuned along with all the other task-specific parameters. Fine-tuning consumes much fewer resources than pre-training, and the pre-trained model is widely available, notably as part of the Transformers library \parencite{Wolf2019HuggingFacesTS}. BERT has achieved state-of-the-art results in many NLP tasks, including Language Understanding, Question Answering, and Natural Language Inference \parencite{devlin2018bert}.

\subsection{RoBERTa and CamemBERT} % (fold)
\label{sub:roberta}
Several variants of BERT have been produced that modify its architecture and training tasks in order to increase its usability across languages and downstream tasks. One of these variants is RoBERTa (Robustly optimised BERT approach) \parencite{liu2019roberta}, which re-trains the BERT model by performing the following modifications:

\begin{enumerate}
	\item Using a dynamic masking pattern, in which each masking pattern in the Masked LM is generated every time a new sequence is fed into the model.
	\item Removing the NSP training task.
	\item Increasing the training batch size.
	\item Replacing embedding models as inputs and instead using Byte-Pair Encoding \parencite{sennrich2015neural}, where sub-word units replace words, and are extracted by performing statistical analysis on the corpus.
\end{enumerate}

RoBERTa achieved even better performance results than BERT, and itself gave way to a family of BERT-like models, including the one we use in this thesis: \textbf{CamemBERT} \parencite{2019arXiv191103894M}, a re-training of RoBERTa on the French part of the OSCAR corpus \parencite{suarez2019asynchronous}. Similarly to RoBERTa, camemBERT creates word and sentence representations in the form of contextual word and sentence embeddings but for French.

\section{Chapter summary} % (fold)
\label{sec:summary_2}
In this chapter we have reviewed previous work on the application of NLP in the legal domain, largely consisting of rule and feature based systems. We have also touched upon the work done in Text Classification and Text Segmentation. Finally, we have introduced the neural methods used in the methodologies proposed in this thesis (see Chapters~\ref{ch:segmentation_sentence_clf} and \ref{ch:segmentation_recurrent}). The next chapter will present the dataset used to train and evaluate our neural models and segmentation method.
% section summary_2 (end)