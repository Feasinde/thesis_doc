\chapter{Summary and future work}
\label{ch:conclusion}

In this chapter, we provide a summary of the approaches used in this thesis to perform Discourse Segmentation of legal documents in order to extract case-relevant \textit{Facts}. We then provide avenues for future work. 

\section{Summary of the thesis} % (fold)
\label{sec:summary_of_the_thesis}

The goal of the thesis was to extract the case relevant facts from texts of judicial rulings by means of neural approaches. To achieve this goal we developed two methods based on Text Segmentation. Both methods rely on the stated assumption from Figure~\ref{assumption}, that facts will be listed and stated before the analysis that produces a decision based on the facts. This thesis was developed as part of the process of building an extrinsic evaluation of the JusticeBot project, developed by the CyberJustice laboratory and the Université de Montréal.

Our first method (see Chapter~\ref{ch:segmentation_sentence_clf}) is based on the independent classification of each sentence in a document and the subsequent segmentation of the document based on the metrics of length and purity. The second method (see Chapter~\ref{ch:segmentation_recurrent}) is based on a recurrent architecture where sentence classification is done contextually, and the subsequent segmentation is also based on length and purity. Both models were trained and tested using an annotated corpus derived from a dataset of legal cases concerning landlord-tenant disputes using the original corpus and an augmented corpus produced with PLSDA.

The first method, the Sentence Classifier method produced the following highlights as part of the intrinsic evaluation: the classifier trained on the original dataset produced an accuracy of 0.79 and an F$_1$ measure of 0.77 for the class \textit{Fact}; the classifiers trained on the augmented datasets performed similarly. As part of the extrinsic evaluation, using the original dataset, the splitting index value that yielded the greatest percentage of sentences offset by fewer than 1 with respect to the gold standard, for all models (base and augmented by 2 and 3) was $\gamma=1.4$.

The second method, the Recurrent Architecture (see Chapter~\ref{ch:segmentation_recurrent}), consisted of 3 distinct recurrent models: an LSTM network, a GRU network, and an Attention Encoder-Decoder. This method produced the following highlights as part of the intrinsic evaluation: overall, the GRU model produced the best performance across the board, with an accuracy of 0.99 and an F$_1$ score of 0.99 on the original dataset, and an accuracy of 0.95 and an F$_1$ score of 0.93 on the augmented dataset. The extrinsic evaluations (see Section~\ref{sec:evaluation_of_the_segmentation_task_recurrent}) produced a smaller percentage of documents whose segmentation point fell short of the point indicated by the gold standard, compared to the Sentence Classifier method, while also considerably increasing the number of documents whose offset from the predicted segmentation point was fewer than 1 sentence.

The second method produced better extrinsic evaluation results than the first, increasing the number of documents whose offset from the gold standard was less than 1 sentence from 5\% to 78\% percent for their best value of the splitting index $\gamma$.

The PLSDA technique of data augmentation (see Section~\ref{sec:data_augmentation}) produced no significant improvements in performance on either methods, which suggests that using more real-world data to train our models might not be particularly important to increase performance.

\section{Future work} % (fold)

\label{sec:future_work}

Following our work, it would be interesting to investigate the following research questions:

\begin{enumerate}
	\item Our work was based on the stated assumption in Figure~\ref{assumption}, which was in turn based on the IRAC legal writing model \parencite{beazley2018practical}. Future work should explore the validation of the assumption, or proposing additional axioms based on more refined models of writing. An implicit assumption we made was that documents from the legal system followed the rule in the province of Québec, which follows both Civil Law and Common Law, instead of solely Common Law like the rest of Canada. It might be the case that these two distinct legal traditions follow different discourse conventions.

	\item Both our methods showed no significant improvement of performance with the augmented dataset. Future work should focus on better text mining techniques in order to obtain a larger training corpus, in order to concretely demonstrate that additional data might not be enough to increase the performance of our models, in particular, the Sentence Classifier.

	\item Additionally, future work might also make exclusive use of contextual word and sentence embeddings, such as the ones produced by BERT and its derivatives, which have consistently shown to outperform non-contextual word embeddings in several benchmarks that use them \parencite{devlin2018bert}. The Sentence Classifier method could be fitted with BERT or a BERT-like component in order to use contextual word embeddings and so take advantage of camemBERT's fine-tuning capabilities; it could also be fitted with an additional encoding module in the form of a camemBERT module with a feedforward linear head in order to enrich the final representation.

	\item Finally, more work needs to be done in terms of model size optimisation in order to incorporate camemBERT's fine-tuning capabilities into the Recurrent Architecture approach. As it stands, the long sequence of sentences typical of any training instance impose a computational challenge that makes it impossible to train camemBERT with our current downstream task. Perhaps techniques such as knowledge distillation \parencite{hinton2015distilling} combined with BERT like models, such as DistilBERT \parencite{sanh2019distilbert}.
\end{enumerate}



% subsection future_work (end)